package eu.telecomnancy.Factory;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorProxy;
import eu.telecomnancy.sensor.SimpleSensorLogger;
import eu.telecomnancy.sensor.TemperatureSensor;

public class SensorProxyFactory extends SensorFactory{
    
    public ISensor getSensor() {
        return new SensorProxy(new TemperatureSensor(), new SimpleSensorLogger());
    }
    
}
