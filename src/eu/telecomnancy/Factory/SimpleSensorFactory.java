package eu.telecomnancy.Factory;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;

public class SimpleSensorFactory {
    
    public ISensor getSensor() {
        return new TemperatureSensor();
    }
    
}
