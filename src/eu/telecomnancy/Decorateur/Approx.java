package eu.telecomnancy.Decorateur;

import eu.telecomnancy.sensor.ISensor; 
import eu.telecomnancy.sensor.SensorNotActivatedException;


public class Approx extends Decorateur{
    
    public Approx(ISensor sensor) {
	super(sensor);
    }
	
    public double getValue() throws SensorNotActivatedException {
	double a = Math.round(sensor.getValue()) ;
        return a;
    }
}
