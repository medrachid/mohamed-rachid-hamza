package eu.telecomnancy.Decorateur;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

public class ConvertFahrenheit extends Decorateur{

	
    public ConvertFahrenheit(ISensor sensor) {            
        super(sensor);
    }
	
    public double getValue() throws SensorNotActivatedException {
	return (sensor.getValue()*1.8+32);
    }

    
}
