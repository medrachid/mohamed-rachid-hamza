
package eu.telecomnancy.Decorateur;

import eu.telecomnancy.sensor.Abcapteur;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;


public abstract class Decorateur extends Abcapteur{
	
    protected ISensor sensor;
	
    public Decorateur(ISensor newSensor) {
	sensor = newSensor;
    }
	
    public double getValue() throws SensorNotActivatedException {
	return sensor.getValue();
    }
    
   public void on() {
	sensor.on();
    }

    public void off() {
	sensor.off();		
    }

    public boolean getStatus() {
	return sensor.getStatus();
    }

    public void update() throws SensorNotActivatedException {
        sensor.update();
		
    }    
    
}