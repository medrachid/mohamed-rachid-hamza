package eu.telecomnancy.State;

import eu.telecomnancy.State.SensorOff;
import eu.telecomnancy.State.SensorOn;
import eu.telecomnancy.State.IState;
import eu.telecomnancy.State.COntext;
import eu.telecomnancy.sensor.Abcapteur;
import eu.telecomnancy.sensor.SensorNotActivatedException;
import java.util.Random;

	 public class State extends Abcapteur{
	     
	     private double value;
	 	
	 private COntext context;
	     
	     public State() {
	        this.context = new COntext();
	     }

	  
	     public void on() {
	         IState on = new SensorOn();
	         on.execute(context);
	     }
	     public void off() {
	         IState off = new SensorOff();
	         off.execute(context);
	         	     }
	     public boolean getStatus() {
	         return context.getState() instanceof SensorOn;
	      
	     }

	     public void update() throws SensorNotActivatedException {
	         if (getStatus()){
	             value = (new Random()).nextDouble() * 100;
	          notifyObserver() ;
	         }
	         else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values");
	     }
		public double getValue() throws SensorNotActivatedException {
	        if (getStatus())
	 	            return value;
	          else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	     }
}
