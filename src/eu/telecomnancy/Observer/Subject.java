package eu.telecomnancy.Observer;

import eu.telecomnancy.sensor.SensorNotActivatedException;


public interface  Subject{
	public void register(Observer o); 
	public void unregister(Observer o);
	public void notifyObserver() throws SensorNotActivatedException;
}
