package eu.telecomnancy.Observer;
import eu.telecomnancy.sensor.SensorNotActivatedException;
public interface  Observer {
public void update() throws SensorNotActivatedException ;
}
