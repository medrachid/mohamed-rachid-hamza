package eu.telecomnancy.sensor;

import java.util.ArrayList;
import eu.telecomnancy.Observer.Observer;
import eu.telecomnancy.Observer.Subject;


public abstract class Abcapteur implements Subject,ISensor {
	
	private ArrayList<Observer> observers ;
	private double valeurcapteur ;
	private LegacyTemperatureSensor capteur ;
     public Abcapteur() {
		observers = new ArrayList<Observer>(); 
		}
     
	public void register(Observer newObserver) {
	observers.add(newObserver) ;		
	}

	public void unregister(Observer deleteObserver) {
		int observerIndex =observers.indexOf(deleteObserver) ;
		observers.remove(observerIndex) ;
	}

	public void notifyObserver() throws SensorNotActivatedException {
		
		for(int i=0;i<observers.size();i++)		{
			Observer o = observers.get(i);
			o.update();
		}
	}

	public void setsensorvalue() throws SensorNotActivatedException{
	this.valeurcapteur = capteur.getTemperature() ;
		notifyObserver() ;
	}
}
