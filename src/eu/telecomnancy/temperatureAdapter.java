package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.LegacyTemperatureSensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;


public class temperatureAdapter implements ISensor{

	private LegacyTemperatureSensor sensor= new LegacyTemperatureSensor()  ;
	
	public temperatureAdapter() {
	}
	public void on() {
		if(!this.sensor.getStatus()) {
			sensor.onOff();
		}	
		
	}
	public void off() {
		if(this.sensor.getStatus()){
		sensor.onOff();
		}
	}
	public boolean getStatus() {
	boolean bool =	sensor.getStatus() ;
		return bool ;
	} 	
	public void update() throws SensorNotActivatedException {		
	}

	public double getValue() throws SensorNotActivatedException {
		return sensor.getTemperature();
	}


}
